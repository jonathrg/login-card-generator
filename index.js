const cardinput = document.getElementById('numcards');
const syllableinput = document.getElementById('syllables');
const cardlist = document.getElementById('cards');
const maxcards = 1000;

function generateUsername(syllables) {
    return chance.word({syllables: syllables});
}

function generatePassword(syllables) {
    return chance.word({syllables: syllables});
}

function makeCredentials() {
    cardlist.innerHTML = "";
    const usernames = [];
    const passwords = [];
    const numCards = Math.min(cardinput.value, maxcards);
    const syllables = Math.min(syllableinput.value, 12);
    for (let i = 0; i < numCards; i++) {
        let username = generateUsername(syllables);
        let password = generatePassword(syllables);

        let elem = document.createElement('div');
        elem.classList.add("card");
        let uname = document.createElement('p');
        let pw = document.createElement('p');
        uname.innerHTML = `<strong>Username</strong>:  ${username}`;
        pw.innerHTML = `<strong>Password</strong>:  ${password}`;

        elem.appendChild(uname);
        elem.appendChild(pw);
        cardlist.appendChild(elem);
    }
}

cardinput.addEventListener('input', function (evt) {
    makeCredentials();
});

syllableinput.addEventListener('input', function (evt) {
    makeCredentials();
});

makeCredentials();
